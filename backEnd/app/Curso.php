<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'curso';
    
    protected $primaryKey = 'id';

    protected $fillable = [
        'nome', 'idProfessor'
    ];

    public function professor() {
        $query = $this->belongsTo('App\Professor', 'idProfessor');

        if($query)
           $query = $query->select('id', 'nome', 'dtNascimento');

        return $query;
    }

}
