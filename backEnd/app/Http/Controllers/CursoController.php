<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Curso;

use Validator;

class CursoController extends Controller
{
    
    private function validarCurso($request) {
        $validator = Validator::make($request->all(), [
            'nome'=> ['required','min:5','max:60']
        ]);
        return $validator;
    }


    public function index()
    {
        try {
            
            $curso = Curso::orderBy('id')
                            ->with('professor')
                            ->get();
            
            return response()->json(['success' => true, 'result'=> $curso], 200);
        
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = $this->validarCurso($request);

            if ($validator->fails()){
                return response()->json(['success' => false, 'result'=> 'Erro', 'errors'=> $validator->errors()], 400);
            }


            $data = $request->all();

            $curso = Curso::create($data);
            
            if ($curso) {
                return response()->json(['success' => true, 'result'=> $curso], 201);
            } else {
                return response()->json(['success' => false, 'result'=>"Erro ao criar a Curso"], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }

    }

    public function show($id)
    {
        try {
            if ($id < 0)
                return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);

            $curso = Curso::orderBy('id')
                            ->with('professor')
                            ->find($id);
            
            if ($curso) {
                return response()->json(['success' => true, 'result'=> $curso], 200);
            } else {
                return response()->json(['success' => false, 'result'=> 'Curso não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            if ($id < 0){
                return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);
            }

            $validator = $this->validarCurso($request);

            if ($validator->fails()){
                return response()->json(['success' => false, 'result'=> 'Erro', 'errors'=> $validator->errors()], 400);
            }

            $data = $request->all();

            $curso = Curso::find($id);
            
            if ($curso) {
                
                $curso->update($data);

                return response()->json(['success' => true, 'result'=> $curso ], 204);

            } else {
                return response()->json(['success' => false, 'result'=> 'Curso não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try {
            if ($id < 0)
            return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);
            
            $curso = Curso::find($id);
            
            if ($curso) {
                
                $curso->delete();

                return response()->json(['success' => true, 'result'=> 'Curso deletado com sucesso' ], 200);

            } else {
                return response()->json(['success' => false, 'result'=> 'Curso não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }
}
