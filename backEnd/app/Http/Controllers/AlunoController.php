<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Aluno;

use Validator;

class AlunoController extends Controller
{
    private function validarAluno($request) {
        $validator = Validator::make($request->all(), [
            'dtNascimento'=> ['required'],
            'logradouro'=> ['required'],
            'bairro'=> ['required'],
            'cidade'=> ['required'],
            'estado'=> ['required'],
            'cep'=> ['required','min:8','max:8'],
            'nome'=> ['required','min:8','max:30']
        ]);
        return $validator;
    }


    public function index()
    {
        try {
            
            $aluno = Aluno::orderBy('id')
                            ->with('curso')
                            ->get();
            
            return response()->json(['success' => true, 'result'=> $aluno], 200);
        
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function relatorio()
    {
        try {
            
            $aluno = Aluno::orderBy('id')
                            ->with('cursoComProfessor')
                            ->get();
            
            return response()->json(['success' => true, 'result'=> $aluno], 200);
        
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = $this->validarAluno($request);

            if ($validator->fails()){
                return response()->json(['success' => false, 'result'=> 'Erro', 'errors'=> $validator->errors()], 400);
            }


            $data = $request->all();

            $aluno = Aluno::create($data);
            
            if ($aluno) {
                return response()->json(['success' => true, 'result'=> $aluno], 201);
            } else {
                return response()->json(['success' => false, 'result'=>"Erro ao cadastrar Aluno"], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }

    }

    public function show($id)
    {
        try {
            if ($id < 0)
                return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);

            $aluno = Aluno::orderBy('id')
                            ->with('curso')
                            ->find($id);
            
            if ($aluno) {
                return response()->json(['success' => true, 'result'=> $aluno], 200);
            } else {
                return response()->json(['success' => false, 'result'=> 'Aluno não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            if ($id < 0){
                return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);
            }

            $validator = $this->validarAluno($request);

            if ($validator->fails()){
                return response()->json(['success' => false, 'result'=> 'Erro', 'errors'=> $validator->errors()], 400);
            }

            $data = $request->all();

            $aluno = Aluno::find($id);
            
            if ($aluno) {
                
                $aluno->update($data);

                return response()->json(['success' => true, 'result'=> $aluno ], 204);

            } else {
                return response()->json(['success' => false, 'result'=> 'Aluno não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try {
            if ($id < 0)
            return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);
            
            $aluno = Aluno::find($id);
            
            if ($aluno) {
                
                $aluno->delete();

                return response()->json(['success' => true, 'result'=> 'Aluno deletado com sucesso' ], 200);

            } else {
                return response()->json(['success' => false, 'result'=> 'Aluno não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }
}
