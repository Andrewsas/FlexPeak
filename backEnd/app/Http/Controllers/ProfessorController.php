<?php

namespace App\Http\Controllers;

use DB; 

use App\Professor;

use Illuminate\Http\Request;

use Validator;

class ProfessorController extends Controller
{

    private function validarProfessor($request) {
        $validator = Validator::make($request->all(), [
            'nome'=> ['required','min:5','max:60'],
            'dtNascimento' => ['required']
        ]);
        return $validator;
    }


    public function index()
    {
        try {
            $professor = Professor::orderBy('id')->get(['id', 'nome', 'dtNascimento'])->toArray();
            
            return response()->json(['success' => true, 'result'=> $professor], 200);
        
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = $this->validarProfessor($request);

            if ($validator->fails()){
                return response()->json(['success' => false, 'result'=> 'Erro', 'errors'=> $validator->errors()], 400);
            }


            $data = $request->all();

            $professor = Professor::create($data);
            
            if ($professor) {
                return response()->json(['success' => true, 'result'=> $professor], 201);
            } else {
                return response()->json(['success' => false, 'result'=>"Erro ao criar a Professor"], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }

    }

    public function show($id)
    {
        try {
            if ($id < 0)
                return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);

            $professor = Professor::find($id);
            
            if ($professor) {
                return response()->json(['success' => true, 'result'=> $professor], 200);
            } else {
                return response()->json(['success' => false, 'result'=> 'Professor não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            if ($id < 0){
                return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);
            }

            $validator = $this->validarProfessor($request);

            if ($validator->fails()){
                return response()->json(['success' => false, 'result'=> 'Erro', 'errors'=> $validator->errors()], 400);
            }

            $data = $request->all();

            $professor = Professor::find($id);
            
            if ($professor) {
                
                $professor->update($data);

                return response()->json(['success' => true, 'result'=> $professor ], 204);

            } else {
                return response()->json(['success' => false, 'result'=> 'Professor não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try {
            if ($id < 0)
            return response()->json(['success' => false,  'result'=>'ID informado é inválido!'], 400);
            
            $professor = Professor::find($id);
            
            if ($professor) {
                
                $professor->delete();

                return response()->json(['success' => true, 'result'=> 'Professor deletado com sucesso' ], 200);

            } else {
                return response()->json(['success' => false, 'result'=> 'Professor não encontrado'], 400);
            }
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'result'=> $e->getMessage()], 500);
        }
    }
}
