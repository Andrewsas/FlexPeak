<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $table = 'aluno';
    
    protected $primaryKey = 'id';

    protected $fillable = [
        'nome', 'dtNascimento','idCurso', 'logradouro', 'numero', 'bairro', 'cidade', 'estado', 'cep'
    ];

    public function curso() {
        $query = $this->belongsTo('App\Curso', 'idCurso');

        if($query)
           $query = $query->select('id', 'nome');

        return $query;
    }

    public function cursoComProfessor() {
        $query = $this->belongsTo('App\Curso', 'idCurso');

        if($query)
           $query = $query->select()->with('professor');

        return $query;
    }
}
