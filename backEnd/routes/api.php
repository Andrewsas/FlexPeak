<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/* Rotas de CRUD de professores */

Route::get('/professores', ['as' => 'professores.listar', 'uses' => 'ProfessorController@index']);

Route::get('/professores/{id}', ['as' => 'professores.detalhes', 'uses' => 'ProfessorController@show']);

Route::post('/professores', ['as' => 'professores.salvar', 'uses' => 'ProfessorController@store']);

Route::put('/professores/{id}', ['as' => 'professores.editar', 'uses' => 'ProfessorController@update']);

Route::delete('/professores/{id}', ['as' => 'professores.deletar', 'uses' => 'ProfessorController@destroy']);


/* Rotas de CRUD de cursos */

Route::get('/cursos', ['as' => 'cursos.listar', 'uses' => 'CursoController@index']);

Route::get('/cursos/{id}', ['as' => 'cursos.detalhes', 'uses' => 'CursoController@show']);

Route::post('/cursos', ['as' => 'cursos.salvar', 'uses' => 'CursoController@store']);

Route::put('/cursos/{id}', ['as' => 'cursos.editar', 'uses' => 'CursoController@update']);

Route::delete('/cursos/{id}', ['as' => 'cursos.deletar', 'uses' => 'CursoController@destroy']);

/* Rotas de CRUD de alunos */

Route::get('/alunos', ['as' => 'alunos.listar', 'uses' => 'AlunoController@index']);

Route::get('/alunos/{id}', ['as' => 'alunos.detalhes', 'uses' => 'AlunoController@show']);

Route::post('/alunos', ['as' => 'alunos.salvar', 'uses' => 'AlunoController@store']);

Route::put('/alunos/{id}', ['as' => 'alunos.editar', 'uses' => 'AlunoController@update']);

Route::delete('/alunos/{id}', ['as' => 'alunos.deletar', 'uses' => 'AlunoController@destroy']);

/* Rotas de CRUD de relatorios */

Route::get('/relatorios', ['as' => 'relatorios.listar', 'uses' => 'AlunoController@relatorio']);