OBS : Esse projeto e resultado de um teste para vaga de Programador Junior,
O mesmo foi realizado usando oo Frameworks Laravel para o Backend e Angular para o FrontEnd.

-- INSTRUÇÕES PARA TESTE EM DESENVOLVIMENTO

-> FRONTEND
    1. Ter o Node e Npm instalado na maquina;
    2. Com o npm instalar o Angular (npm install -g @angular/cli)
    3. Baixar o projeto frontEnd
    4. Dentro do diretorio baixar as dependencias com o npm (npm install)
    5. para rodar a aplicação executar (npm install)
        obs: o projeto front está apontando para http://127.0.0.1:8080/api, para mudar o endereço das requisições editar o environments
    6. Para buildar o projeto para produção (npm build --prod)

-> BACKTEND
    1. Ter o php, composer e o Mysql instalado;
    2. instalar o laravel (composer global require "laravel/installer");
    3. Baixar o projeto backEnd
    4. configurar o banco no arquivo 'config/database/php'
        obs: Todas as configurações de Banco devem ser feitas via variavel de ambiente e o cors.php ajustados para segurança do servidor.
    5. Depois dentro do diretorio execurtar as migrations (php artisan migrate)
    6. Para levantar o backend (php artisan serve)

-- REQUISITOS DO TESTE

Dica: Quanto mais detalhe, seja ela de front-end ou nova funcionalidade, melhor para o seu processo.

1. Criar CRUD de 3 tabelas listadas abaixo em PHP: 

TABELA
- ALUNO
    + ID_ALUNO
    + NOME
    + DATA_NASCIMENTO
    + LOGRADOURO
    + NUMERO
    + BAIRRO
    + CIDADE
    + ESTADO
    + DATA_CRIACAO
    + CEP
    + ID_CURSO

- CURSO
    + ID_CURSO
    + NOME
    + DATA_CRIACAO
    + ID_PROFESSOR

- PROFESSOR
    + ID_ PROFESSOR
    + NOME
    + DATA_NASCIMENTO
    + DATA_CRIACAO


2. Fazer uma consulta webservice nos correios para preencher os endereços a partir do CEP. Sugestões abaixo: 

http://postmon.com.br/

https://viacep.com.br/ 

3. Gerar um relatório em PDF contendo os alunos cadastrados, qual curso ele pertence e quem é o professor dele. 

4. Fazer versionamento do projeto em algum serviço git como github, bitbucket ou gitlab.

5. Colocar o projeto na nuvem utilizando algum serviço de hospedagem (pode ser hospedagem gratúita). 