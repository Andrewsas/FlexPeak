import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from '../../service/request.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastro-curso',
  templateUrl: './cadastro-curso.component.html',
  styleUrls: ['./cadastro-curso.component.css']
})
export class CadastroCursoComponent implements OnInit {

  constructor(
    private request: RequestService,
    private route: ActivatedRoute
  ) {
    let consultaProfessores = request.getProfessores().subscribe(
      (data: any) => {
        if (data.success) {
          this.listProfessores = data.result;
        }
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaProfessores.unsubscribe();
      }
    );
  }

  ngOnInit() {
    let sub = this.route
      .queryParams
      .subscribe(params => {
        this.idCurso = params['id'] || 0;
        if (this.idCurso) {
          this.carregaCurso(this.idCurso);
        }
      }, (error) => { },
        () => {
          sub.unsubscribe();
        }
      );
  }
  public idCurso: number = 0;

  public curso: any = {};

  public listProfessores: any[] = [];

  public alert = {
    type: undefined,
    msg: undefined
  };

  public formCurso = new FormGroup({
    nome: new FormControl('', [Validators.required, Validators.minLength(5)]),
    idProfessor: new FormControl('0', [Validators.required, Validators.min(1)])
  });

  public cadastrarCurso() {
    if (this.formCurso.valid) {
      let cadatraCurso = this.request.setCursos(this.formCurso.value).subscribe(
        (data: any) => {
          if (data.success) {
            this.alert = {
              type: 'success',
              msg: 'Cadastro realizado com sucesso.'
            }
            this.formCurso.reset();
          }
        }, (error: any) => {
          console.log(error);
        }, () => {
          cadatraCurso.unsubscribe();
        }
      );
    }
  }

  public editarCurso() {
    if (this.formCurso.valid) {
      let cadatraCurso = this.request.setCursos(this.formCurso.value, this.idCurso).subscribe(
        (data: any) => {
            this.alert = {
              type: 'success',
              msg: 'Dados Atualizados com sucesso.'
            }
        }, (error: any) => {
          console.log(error);
        }, () => {
          cadatraCurso.unsubscribe();
        }
      );
    }
  }

  public carregaCurso(idCurso) {
    let consultaCurso = this.request.getCursos(idCurso).subscribe(
      (data: any) => {
        this.formCurso.get('nome').setValue(data.result.nome);
        this.formCurso.get('idProfessor').setValue(data.result.professor.id);
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaCurso.unsubscribe();
      }
    );
  }

}
