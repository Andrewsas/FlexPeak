import { Component, OnInit } from '@angular/core';
import { RequestService } from '../service/request.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.css']
})
export class CursoComponent implements OnInit {

  constructor(
    private request: RequestService,
    private modalService: NgbModal
  ) {
    this.carregaCursos();
  }

  ngOnInit() {
  }

  public editCurso: boolean = false;

  public pagina: number = 1;

  public listCursos: any[] = [];

  public cursos: any[] = [];


  public cursoDelete: any = {};

  public open(content, curso) {
    this.cursoDelete = curso;
    console.log(this.cursoDelete);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.delete(curso.id);
    }, (reason) => {
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public delete(id) {
    let deletandoCurso = this.request.delCursos(id).subscribe(
      (data: any) => {
        if(data.success){
          this.carregaCursos();
        }
      }, (error: any) => {
        console.log(error);
      }, () => {
        deletandoCurso.unsubscribe();
      }
    );
  }

  public mudaPagina(index) {
    this.listCursos = [];
    for (let i = (index * 10) - 10; (i < index * 10) && (i < this.cursos.length); i++) {
      this.listCursos.push(this.cursos[i]);
    }
  }

  private carregaCursos(){
    let consultaProfessores = this.request.getCursos().subscribe(
      (data: any) => {
        if (data.success) {
          this.cursos = data.result;
          this.mudaPagina(this.pagina);
        }
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaProfessores.unsubscribe();
      }
    );
  }

}
