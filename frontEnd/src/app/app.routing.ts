import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AlunoComponent } from './aluno/aluno.component';
import { ProfessorComponent } from './professor/professor.component';
import { CursoComponent } from './curso/curso.component';
import { CadastroProfessorComponent } from './professor/cadastro-professor/cadastro-professor.component';
import { CadastroCursoComponent } from './curso/cadastro-curso/cadastro-curso.component';
import { CadastroAlunoComponent } from './aluno/cadastro-aluno/cadastro-aluno.component';
import { RelatorioComponent } from './relatorio/relatorio.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [{
            path: '',
            component: HomeComponent,
            children: [{
                path: '',
                component: AlunoComponent
            }, {
                path: 'aluno',
                component: AlunoComponent
            }, {
                path: 'cadastroAluno',
                component: CadastroAlunoComponent
            }, {
                path: 'professor',
                component: ProfessorComponent,
            }, {
                path: 'cadastroProfessor',
                component: CadastroProfessorComponent
            }, {
                path: 'curso',
                component: CursoComponent
            }, {
                path: 'cadastroCurso',
                component: CadastroCursoComponent
            }, {
                path: 'relatorio',
                component: RelatorioComponent
            }]
        }]
    }, {
        path: '**',
        redirectTo: '',
    }
];
