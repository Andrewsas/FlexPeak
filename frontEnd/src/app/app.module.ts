import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AlunoComponent } from './aluno/aluno.component';
import { ProfessorComponent } from './professor/professor.component';
import { CursoComponent } from './curso/curso.component';
import { AppRoutes } from './app.routing';
import { RouterModule } from '@angular/router';
import { CadastroProfessorComponent } from './professor/cadastro-professor/cadastro-professor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequestService } from './service/request.service';
import { CadastroCursoComponent } from './curso/cadastro-curso/cadastro-curso.component';
import { HttpClientModule } from '@angular/common/http';
import { CadastroAlunoComponent } from './aluno/cadastro-aluno/cadastro-aluno.component';
import { RelatorioComponent } from './relatorio/relatorio.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AlunoComponent,
    ProfessorComponent,
    CursoComponent,
    CadastroProfessorComponent,
    CadastroCursoComponent,
    CadastroAlunoComponent,
    RelatorioComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(AppRoutes),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule 
  ],
  providers: [
    RequestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
