import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class RequestService {

    constructor(public http: HttpClient) { }

    private headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
    })

    private options = {
        'headers': this.headers
    };

    private urlEndPoints = {
        urlAluno: '/alunos',
        urlCurso: '/cursos',
        urlRelatorios: '/relatorios',
        urlProfessor: '/professores',
        urlCep: 'https://viacep.com.br/ws/{0}/json/'
    }

    //Consultas nos endpoints
    public getAlunos(id?) {
        if (id) {
            return this.http.get(environment.path + this.urlEndPoints.urlAluno + '/' + id);
        }
        return this.http.get(environment.path + this.urlEndPoints.urlAluno);
    }

    public getProfessores(id?) {
        if (id) {
            return this.http.get(environment.path + this.urlEndPoints.urlProfessor + '/' + id);
        }
        return this.http.get(environment.path + this.urlEndPoints.urlProfessor);
    }

    public getCursos(id?) {
        if (id) {
            return this.http.get(environment.path + this.urlEndPoints.urlCurso + '/' + id);
        }
        return this.http.get(environment.path + this.urlEndPoints.urlCurso);
    }
    public getRelatorio() {
      return this.http.get(environment.path + this.urlEndPoints.urlRelatorios);
  }

    //consulta cep
    public getCep(cep: string) {
        let url = this.urlEndPoints.urlCep.replace('{0}', cep);
        return this.http.get(url);
    }

    //endpoint de cadastro e atualizacao
    public setAlunos(body, id?) {
        if (id) {
            return this.http.put(environment.path + this.urlEndPoints.urlAluno + '/' + id, body);
        }
        return this.http.post(environment.path + this.urlEndPoints.urlAluno, body);
    }

    public setProfessores(body, id?) {
        if (id) {
            return this.http.put(environment.path + this.urlEndPoints.urlProfessor + '/' + id, body);
        }
        return this.http.post(environment.path + this.urlEndPoints.urlProfessor, body);
    }

    public setCursos(body, id?) {
        if (id) {
            return this.http.put(environment.path + this.urlEndPoints.urlCurso + '/' + id, body);
        }
        return this.http.post(environment.path + this.urlEndPoints.urlCurso, body);
    }

    // endpoit de delete
    public delAlunos(id?) {
        return this.http.delete(environment.path + this.urlEndPoints.urlAluno + '/' + id);
    }

    public delProfessores(id?) {
        return this.http.delete(environment.path + this.urlEndPoints.urlProfessor + '/' + id);
    }

    public delCursos(id?) {
        return this.http.delete(environment.path + this.urlEndPoints.urlCurso + '/' + id);
    }

}
