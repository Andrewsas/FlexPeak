import { Component, OnInit } from '@angular/core';
import { RequestService } from '../service/request.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.css']
})
export class ProfessorComponent implements OnInit {

  constructor(
    private request: RequestService,
    private modalService: NgbModal
  ) {
    this.carregaProfessor();
  }

  ngOnInit() {

  }

  public pagina: number = 1;
  public listProfessor: any[] = [];
  public professores: any[] = [];

public professorDelete: any = {}

  public open(content, professor) {
    this.professorDelete = professor;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.delete(professor.id);
    }, (reason) => {
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public delete(id) {
    let deletandoProfessor = this.request.delProfessores(id).subscribe(
      (data: any) => {
        if (data.success) {
          this.carregaProfessor();
        }
      }, (error: any) => {
        console.log(error);
      }, () => {
        deletandoProfessor.unsubscribe();
      }
    );
  }

  public mudaPagina(index){
    this.listProfessor = [];
    let pageAlunos:any[] = [];
    for(let i = (index * 10) - 10 ; (i < index * 10) && (i < this.professores.length); i++){
      this.listProfessor.push(this.professores[i]);
    }
  }

  private carregaProfessor(){
    let consultaProfessores = this.request.getProfessores().subscribe(
      (data: any) => {
        this.professores = data.result;
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaProfessores.unsubscribe();
        this.mudaPagina(this.pagina);
      }
    );
  }

}
