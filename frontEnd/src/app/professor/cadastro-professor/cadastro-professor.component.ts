import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from '../../service/request.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastro-professor',
  templateUrl: './cadastro-professor.component.html',
  styleUrls: ['./cadastro-professor.component.css']
})
export class CadastroProfessorComponent implements OnInit {

  constructor(
    private request: RequestService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    let sub = this.route
      .queryParams
      .subscribe(params => {
        this.idProfessor = params['id'] || 0;
        if (this.idProfessor) {
          this.carregaProfessor(this.idProfessor);
        }
      }, (error) => { },
        () => {
          sub.unsubscribe();
        });
  }

  public alert = {
    type: undefined,
    msg: undefined
  };

  public idProfessor: number = 0;

  public formProfessor = new FormGroup({
    nome: new FormControl('', [Validators.required, Validators.minLength(5)]),
    dtNascimento: new FormControl('', [Validators.required])
  });

  public cadastrarProfessor() {
    if (this.formProfessor.valid) {
      let cadatraProfessor = this.request.setProfessores(this.formProfessor.value).subscribe(
        (data: any) => {
          if (data.success) {
            this.alert = {
              type: 'success',
              msg: 'Cadastro realizado com sucesso.'
            }
            this.formProfessor.reset();
          }
        }, (error: any) => {
          console.log(error);
        }, () => {
          cadatraProfessor.unsubscribe();
        }
      );
    }
  }

  public editarProfessor() {
    if (this.formProfessor.valid) {
      let cadatraProfessor = this.request.setProfessores(this.formProfessor.value, this.idProfessor).subscribe(
        (data: any) => {
          this.alert = {
            type: 'success',
            msg: 'Dados Atualizados com sucesso.'
          }
        }, (error: any) => {
          console.log(error);
        }, () => {
          cadatraProfessor.unsubscribe();
        }
      );
    }
  }

  public carregaProfessor(idProfessor) {
    let consultaProfessor = this.request.getProfessores(idProfessor).subscribe(
      (data: any) => {
        this.formProfessor.get('nome').setValue(data.result.nome);
        this.formProfessor.get('dtNascimento').setValue(data.result.dtNascimento);
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaProfessor.unsubscribe();
      }
    );
  }

  public closeAlert() {
    this.alert = {
      type: undefined,
      msg: undefined
    }
  }


}
