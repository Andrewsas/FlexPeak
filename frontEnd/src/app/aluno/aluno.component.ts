import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RequestService } from '../service/request.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as jsPDF from 'jspdf'

@Component({
  selector: 'app-aluno',
  templateUrl: './aluno.component.html',
  styleUrls: ['./aluno.component.css']
})
export class AlunoComponent implements OnInit {


  openModal: boolean = false;

  constructor(
    private request: RequestService,
    private modalService: NgbModal
  ) {
    this.carregaAlunos();
  }

  ngOnInit() {

  }

  public pagina: number = 1;

  public alunos: any[] = [];

  public listAlunos: any[] = [];

  public alunoDelete: any = {};

  public doc = new jsPDF({
    orientation: 'landscape',
    unit: 'in',
    format: [4, 2]
  });

  teste(relatorio) {
    console.log('relatorio', relatorio);
    this.doc.fromHTML(relatorio, 15, 15, {
      'width': 170
    });

    //this.doc.save('two-by-four.pdf')
  }

  public open(content, aluno) {
    this.alunoDelete = aluno;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.delete(aluno.id);
    }, (reason) => {
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public delete(id) {
    let deletandoAluno = this.request.delAlunos(id).subscribe(
      (data: any) => {
        if(data.success){
          this.carregaAlunos();
        }
      }, (error: any) => {
        console.log(error);
      }, () => {
        deletandoAluno.unsubscribe();
      }
    );
  }

  public mudaPagina(index) {
    this.listAlunos = [];
    for (let i = (index * 10) - 10; (i < index * 10) && (i < this.alunos.length); i++) {
      this.listAlunos.push(this.alunos[i]);
    }
  }

  private carregaAlunos() {
    let consultaProfessores = this.request.getAlunos().subscribe(
      (data: any) => {
        this.alunos = data.result;
        this.mudaPagina(this.pagina);
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaProfessores.unsubscribe();
      }
    );
  }

}
