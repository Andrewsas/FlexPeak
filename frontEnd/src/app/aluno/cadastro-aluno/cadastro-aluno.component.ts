import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from '../../service/request.service';
import { estados } from '../../../environments/estados';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject, merge, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastro-aluno',
  templateUrl: './cadastro-aluno.component.html',
  styleUrls: ['./cadastro-aluno.component.css']
})
export class CadastroAlunoComponent implements OnInit {

  constructor(
    private request: RequestService,
    private route: ActivatedRoute,
  ) {
    let consultaCursos = request.getCursos().subscribe(
      (data: any) => {
        this.listCursos = data.result;
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaCursos.unsubscribe();
      }
    );
  }

  ngOnInit() {
    let sub = this.route
      .queryParams
      .subscribe(params => {
        this.idAluno = params['id'] || 0;
        if (this.idAluno) {
          this.carregaAluno(this.idAluno);
        }
      }, (error) => { },
        () => {
          sub.unsubscribe();
        });
  }

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  public idAluno: number = 0;

  public listCursos: any[] = [];

  public estados: any[] = estados.estadoArray;

  public alert = {
    type: undefined,
    msg: undefined
  };

  public formAluno = new FormGroup({
    nome: new FormControl('', [Validators.required, Validators.minLength(5)]),
    dtNascimento: new FormControl('', [Validators.required]),
    idCurso: new FormControl('', [Validators.required, Validators.min(1)]),
    logradouro: new FormControl('', [Validators.required, Validators.minLength(5)]),
    numero: new FormControl('', []),
    estado: new FormControl('', [Validators.required]),
    cep: new FormControl('', [Validators.required]),
    cidade: new FormControl('', [Validators.required]),
    bairro: new FormControl('', [Validators.required]),
  });

  public cadastrarAluno() {
    if (this.formAluno.valid) {
      let cadatraCurso = this.request.setAlunos(this.formAluno.value).subscribe(
        (data: any) => {
          if (data.success) {
            this.alert = {
              type: 'success',
              msg: 'Cadastro realizado com sucesso.'
            }
            this.formAluno.reset();
          }
        }, (error: any) => {
          console.log(error);
        }, () => {
          cadatraCurso.unsubscribe();
        }
      );
    }
  }

  public editarAluno() {
    if (this.formAluno.valid) {
      let cadatraCurso = this.request.setAlunos(this.formAluno.value, this.idAluno).subscribe(
        (data: any) => {
          this.alert = {
            type: 'success',
            msg: 'Dados Atualizados com sucesso.'
          }
        }, (error: any) => {
          console.log(error);
        }, () => {
          cadatraCurso.unsubscribe();
        }
      );
    }
  }

  public verificaCep() {
    let cep: string = this.formAluno.get('cep').value;
    if (cep.length == 8) {
      let carregaEndereco = this.request.getCep(cep).subscribe(
        (data: any) => {
          this.formAluno.get('bairro').setValue(data.bairro);
          this.formAluno.get('logradouro').setValue(data.logradouro);
          this.formAluno.get('cidade').setValue(data.localidade);
          estados.estadoJson.forEach((obj, i) => {
            if (obj.sigla == data.uf) {
              this.formAluno.get('estado').setValue(obj.nome);
            }
          });

        }, (error: any) => {
          console.log(error);
        }, () => {
          carregaEndereco.unsubscribe();
        }
      );
    }
  }

  public search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.estados
        : this.estados.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );

  }

  public carregaAluno(idAluno) {
    let consultaCurso = this.request.getAlunos(idAluno).subscribe(
      (data: any) => {
        this.formAluno.get('nome').setValue(data.result.nome);
        this.formAluno.get('dtNascimento').setValue(data.result.dtNascimento);
        this.formAluno.get('idCurso').setValue(data.result.curso.id);
        this.formAluno.get('logradouro').setValue(data.result.logradouro);
        this.formAluno.get('numero').setValue(data.result.numero);
        this.formAluno.get('estado').setValue(data.result.estado);
        this.formAluno.get('cep').setValue(data.result.cep);
        this.formAluno.get('cidade').setValue(data.result.cidade);
        this.formAluno.get('bairro').setValue(data.result.bairro);
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaCurso.unsubscribe();
      }
    );
  }

}


