import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as jsPDF from 'jspdf'
import { elementEnd } from '../../../node_modules/@angular/core/src/render3/instructions';
import { RequestService } from '../service/request.service';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  constructor(private request: RequestService) {
    this.carregaRelatorio();
  }

  ngOnInit() {

  }

  @ViewChild('content') content: ElementRef;

  public listRelatorio: any[] = [];

  public dowloadPdf() {
    let doc = new jsPDF();

    let specialElement = {
      '#editor': (element, renderer) => {
        return true;
      }
    };

    let content = this.content.nativeElement;

    doc.fromHTML(content.innerHTML, 15, 15, {
      'whidth': 190,
      'elementHandlers': specialElement
    });

    doc.save('Relatorio_de_Matriculas.pdf');
  }

  private carregaRelatorio() {
    let consultaProfessores = this.request.getRelatorio().subscribe(
      (data: any) => {
        this.listRelatorio = data.result;
      }, (error: any) => {
        console.log(error);
      }, () => {
        consultaProfessores.unsubscribe();
      }
    );
  }


}
